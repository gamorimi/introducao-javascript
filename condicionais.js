/*
let idade = 23;
if(idade <10){
    console.log("Jovem");
}else if(idade >=10 && idade <=20){
    console.log("Jovem");
}else{
    console.log("Velho");
}
*/

let numero = 11;
while(numero <=10){
    console.log(`numero é ${numero}`);
    //console.log("numero é " + numero);
    numero++;
}

numero = 11;
do{
    console.log(`numero é ${numero}`);
    numero++;
}while(numero <=10);

for(let contador=0; contador<=10; contador++){
    console.log(`numero é ${contador}`);
}